import scrapy
from urlparse import urlparse
from datetime import datetime
from upstream.items import UpstreamItem

class UpstreamDbSpider(scrapy.Spider):
    name = "upstream"
    allowed_domains = ['upstreamdb.com']

    def __init__(self, directory='', *args, **kwargs):
        super(UpstreamDbSpider, self).__init__(*args, **kwargs)
        self.start_urls = ['https://www.upstreamdb.com/%s' % directory]

    def parse(self, response):
        item = UpstreamItem()
        yield self._set_content_data(response, item)

        selectors = [ 'div.tab-pane a:not([rel="nofollow"])::attr(href)',
                       'table#operator_wells a:not([rel="nofollow"])::attr(href)',
                       'table#top_operators_oil a:not([rel="nofollow"])::attr(href)',
                       'table#top_operators_gas a:not([rel="nofollow"])::attr(href)',
                       'div.page-content div.col-lg-3 a:not([rel="nofollow"])::attr(href)',
                       'div.page-content div.col-lg-9 a:not([rel="nofollow"])::attr(href)', ]
        for selector in selectors:
            child_pages = response.css(selector).extract()
            if len(child_pages) > 0:
                for child_page in child_pages:
                    yield scrapy.Request(response.urljoin(child_page), callback=self.parse)

    def _set_content_data(self, response, item):
        parsed_url = urlparse(response.url)
        query_string = '?' + parsed_url.query if parsed_url.query else ''

        item['url'] = response.url
        item['hostname'] = parsed_url.netloc
        item['protocol'] = parsed_url.scheme
        item['request_uri'] = '%s%s' % (parsed_url.path, query_string)
        item['canonical'] = '%s://%s%s' % (item['protocol'], item['hostname'], item['request_uri'])
        item['depth'] = response.meta['depth']
        item['incoming_links'] = 0
        item['outgoing_links'] = 0
        item['title'] = response.css('h1.page-title::text').extract_first()
        item['description'] = response.xpath('/html/head/meta[@name="description"]/@content').extract_first()
        item['status_code'] = response.status
        item['referrer'] = response.request.headers.get('Referer')
        item['date'] = datetime.now()

        return item
