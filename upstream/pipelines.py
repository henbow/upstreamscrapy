# -*- coding: utf-8 -*-

from datetime import datetime
from pony.orm.core import *
from scrapy.exceptions import DropItem
from upstream.models import CrawlLogs


class UpstreamValidatePipeline(object):
    def process_item(self, item, spider):
        if item['title'] == None:
            raise DropItem('Title can not be empty!')

        if item['description'] == None:
            raise DropItem('Description can not be empty!')

        return item


class UpstreamSaveDbPipeline(object):
    def process_item(self, item, spider):
        if self._existing_item(item['url']):
            item['incoming_links'] = self._last_incoming_links(item['url']) + 1
            item['outgoing_links'] = self._calculate_outgoing_links(item['url'])
            self._update_links_count(item['url'], item['incoming_links'], item['outgoing_links'])
        else:
            self._save_logs(item)

        return item

    @db_session
    def _existing_item(self, url):
        return CrawlLogs.exists(url=url)

    @db_session
    def _last_incoming_links(self, url):
        log = CrawlLogs.get(url=url)
        return log.incoming_links

    @db_session
    def _calculate_outgoing_links(self, url):
        return count(c for c in CrawlLogs if c.referrer == url)

    @db_session
    def _update_links_count(self, url, total_in_links, total_out_links):
        log = CrawlLogs.get(url=url)
        log.incoming_links = total_in_links
        log.outgoing_links = total_out_links

    @db_session
    def _save_logs(self, item):
        return CrawlLogs(**item)
