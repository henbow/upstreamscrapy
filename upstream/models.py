from pony.orm import *
from upstream import dbconfig
from datetime import datetime

db = Database()

class CrawlLogs(db.Entity):
    _table_ = "crawl_logs"

    id = PrimaryKey(int, auto=True)
    url = Required(str)
    hostname = Required(str)
    protocol = Required(str)
    request_uri = Required(str)
    canonical = Required(str)
    depth = Required(int)
    incoming_links = Optional(int)
    outgoing_links = Optional(int)
    title = Required(str)
    description = Optional(str)
    status_code = Optional(int)
    referrer = Optional(str)
    date = Optional(datetime)

db.bind('postgres', **dbconfig())
db.generate_mapping(create_tables=True)
