# -*- coding: utf-8 -*-

import scrapy


class UpstreamItem(scrapy.Item):
    url = scrapy.Field()
    hostname = scrapy.Field()
    protocol = scrapy.Field()
    request_uri = scrapy.Field()
    canonical = scrapy.Field()
    depth = scrapy.Field()
    incoming_links = scrapy.Field()
    outgoing_links = scrapy.Field()
    title = scrapy.Field()
    description = scrapy.Field()
    status_code = scrapy.Field()
    referrer = scrapy.Field()
    date = scrapy.Field()
