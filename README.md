# UpstreamDB Scraper #

I have tested this scraper in Linux Ubuntu 14.04. It should be working on Mac machine. To install on Mac and Ubuntu (include all ubuntu-based distros) just follow below steps.

## Requirements ##
- Python 2.7
- Python PIP 9.0.1
- Virtualenv
- PostgreSQL

## Installation ##
1. Create new virtual environment by running this command: `$ mkdir -p ~/.virtualenvs && virtualenv ~/.virtualenvs/upstream-env`
2. Activate virtualenv: `$ source ~/.virtualenvs/upstream-env/bin/activate`
3. Install dependencies: `$ pip install -r requirements.txt`
4. Edit file *database.ini* in the scraper root directory with the correct values:
```
[postgresql]
host=localhost
database=<dbname>
user=<user>
password=<password>
```

## How to run ##
1. Go to the scraper root directory.
2. Run this command `$ scrapy crawl upstream` or `$ scrapy crawl upstream -a directory="<directory_name>"` to run spider for specific directory.

That's it!

